package org.example;

public interface Action {
    double execute(double num1, double num2);
}
