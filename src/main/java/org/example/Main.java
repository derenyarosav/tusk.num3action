package org.example;

public class Main {
    public static void main(String[] args) {
        Addition addition = new Addition();
        Subtraction subtraction = new Subtraction();
        Multiplication multiplication = new Multiplication();
        Division division = new Division();
        System.out.println(addition.execute(23.9,56));
        System.out.println(subtraction.execute(65.8, 34.9));
        System.out.println(multiplication.execute(45,76));
        System.out.println(division.execute(55,5));

    }
}